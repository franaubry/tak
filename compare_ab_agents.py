import traceback

def compare_successors(state, correct, other):
  other_successors = [ x for x in other.successors(state.copy()) ]
  correct_successors = [ x for x in correct.successors(state.copy()) ]
  if len(other_successors) != len(correct_successors):
    return 'Your sucessor function is wrong on state:\n\n{0}\n\nExpected {1} successors but got {2}\n\nYou can use the visualisation tool to see the states'.format(state.get_inginious_str(), len(correct_successors), len(other_successors))
  if other_successors != correct_successors:
    bad_i = -1
    for i in range(len(correct_successors)):
      if other_successors[i] != correct_successors[i]:
        bad_i = i
        break
    return 'Your successor function is wrong on state:\n\n{0}\n\nThe {1}-th successor is supposed to be {2}but you gave {3}\n\nYou can use the visualisation tool to see the states'.format(state.get_inginious_str(), i, correct_successors[i][1].get_inginious_str(), other_successors[i][1].get_inginious_str())
  return 'ok'

def compare_evaluation(state, correct, other):
  other_eval = other.evaluate(state.copy())
  correct_eval = correct.evaluate(state.copy())
  if other_eval != correct_eval:
    return 'Your evaluation function is wrong on state:\n\n{0}\n\nThe expected evaluation is {1} but you gave {2}\n\nYou can use the visualisation tool to see the states'.format(state.get_inginious_str(), correct_eval, other_eval)
  other_eval = [ other.evaluate(s) for _, s in other.successors(state.copy()) ]
  correct_eval = [ correct.evaluate(s) for _, s in correct.successors(state.copy()) ]
  assert len(other_eval) == len(correct_eval)
  if other_eval != correct_eval:
    bad_i = -1
    for i in range(len(correct_eval)):
      if other_eval[i] != correct_eval[i]:
        bad_i = i
        break
    return 'Your successor function is wrong on the {0}-th successor of state:\n\n{1}\n\nThe expected evaluation is {2} but you gave {3}\n\nYou can use the visualisation tool to see the states'.format(i, state.get_inginious_str(), correct_eval[i], other_eval[i])
  return 'ok'    

def compare_cutoff(state, correct, other, max_depth):
  for depth in range(max_depth):
    correct_cutoff = correct.cutoff(state.copy(), depth)
    other_cutoff = other.cutoff(state.copy(), depth)
    if correct_cutoff != other_cutoff:
      return 'Your cutoff function is wrong at depth {0} for state:\n\n{1}The correct cutoff was {2} but you gave {3}\n\nYou can use the visualisation tool to see the states'.format(depth, state.get_inginious_str(), correct_cutoff, other_cutoff)
  return 'ok'

def compare_action(state, correct, other, last_action, time_left):
  other_action = other.get_action(state.copy(), last_action, time_left)
  correct_action = correct.get_action(state.copy(), last_action, time_left)
  if other_action != correct_action:
    return 'Your agent provided the wrong action on state:\n\n{0}\n\nExpected action {1} but got action {2}\n\nYou can use the visualisation tool to see the states'.format(state.get_inginious_str(), correct_action, other_action)
  return 'ok'
    
def compare(initial_state, correct, other, adversary, player_id, max_depth, time_left):
  correct.set_id(player_id)
  other.set_id(player_id)
  adversary.set_id(1 - player_id)
  state = initial_state.copy()
  last_action = None
  try:
    while not state.game_over_check():
      status = compare_successors(state, correct, other)
      if status != 'ok': return status
      status = compare_evaluation(state, correct, other)
      if status != 'ok': return status
      status = compare_cutoff(state, correct, other, max_depth)
      if status != 'ok': return status
      status = compare_action(state, correct, other, last_action, time_left)
      if status != 'ok': return status
      action = correct.get_action(state, last_action, time_left)
      state.apply_action(action)
      if state.game_over_check(): break
      last_action = action
      action = adversary.get_action(state, last_action, time_left)
      state.apply_action(action)
      last_action = action
  except Exception as err:
    msg = '**Your agent crashed!**\n\n'
    msg += '\n\n'
    tmp = traceback.format_exc().split('\n')
    msg += '\t\t**' + ' '.join(tmp[-4].strip().split(' ')[2:]) + '**\n\n'
    msg += '\t\t**' + tmp[-3].strip() + '**\n\n'
    msg += '\t\t**' + tmp[-2].strip() + '**\n\n'
    return msg
  return 'ok'
