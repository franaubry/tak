from agent import AlphaBetaAgent
from random import shuffle
import minimax

"""
Agent skeleton. Fill in the gaps.
"""
class MyAgent(AlphaBetaAgent):

  """
  This is the skeleton of an agent to play the Tak game.
  """
  def get_action(self, state, last_action, time_left):
    self.last_action = last_action
    self.time_left = time_left
    return minimax.search(state, self)

  """
  The successors function must return (or yield) a list of
  pairs (a, s) in which a is the action played to reach the
  state s.
  """
  def successors(self, state):
    actions = state.get_current_player_actions()
    for a in actions:
      statec = state.copy()
      statec.apply_action(a)
      yield a, statec

  """
  The cutoff function returns true if the alpha-beta/minimax
  search has to stop and false otherwise.
  """
  def cutoff(self, state, depth):
    return state.game_over_check() or depth == 2

  """
  The evaluate function must return an integer value
  representing the utility function of the board.
  """
  def evaluate(self, state):
    if state.game_over_check():
      if state.get_winner() == self.id: return 1000
      return -1000
    c = state.control_count()
    return c[self.id] - c[1 - self.id]
