from agent import AlphaBetaAgent
import minimax
import heapq
from tak import *

N = 5

INF = 100000
DELTA = [ (-1,0), (1,0), (0,-1), (0,1) ]
TOP =    [ (0, c) for c in range(N) ]
BOTTOM = [ (N - 1, c) for c in range(N) ]

LEFT = [ (r, 0) for r in range(N) ]
RIGHT = [ (r, N - 1) for r in range(N) ]

class MyAgent(AlphaBetaAgent):

  def get_name(self):
    return 'greedy path agent'

  def get_action(self, state, last_action, time_left):
    return minimax.search(state, self)

  def successors(self, state):
    actions = state.get_current_player_actions()
    S = [ ]
    i = 0
    for action in actions:
      statec = state.copy()
      statec.apply_action(action)
      S.append( (self.evaluate(statec), i, action, statec) )
      i += 1
    #S.sort()
    return [ (S[i][2], S[i][3]) for i in range(len(S)) ]

  def cutoff(self, state, depth):
    return state.game_over() or depth == 1
    
  def evaluate(self, state):
    if state.game_over():
      if state.get_winner() == self.id: return 1000
      return -1000
    my_vertical = vertical_dist(state, self.id)
    my_horizontal = horizontal_dist(state, self.id)

    his_vertical = vertical_dist(state, 1 - self.id)
    his_horizontal = horizontal_dist(state, 1 - self.id)

    my_best = N - min(my_vertical, my_horizontal)
    his_best = N - min(his_vertical, his_horizontal)

    delta_path = my_best - his_best
    sgn = 1
    if delta_path < 0:
      sgn = -1

    c = state.control_count()
    delta_territory = c[self.id] - c[1 - self.id]

    delta_cap = state.capstones[self.id] - state.capstones[1 - self.id]

    delta_stones = -(state.stones[self.id] - state.capstones[1 - self.id])

    return sgn * delta_path * delta_path + delta_territory + 5 * delta_cap + delta_stones
   

"""
Compute the minimum number of pieces needed for the given palyer
to connect the bottom and the top.
"""
def top_bottom_distance(state, player):
  n = state.get_size()
  cap = STONES[n][1]
  dist = [ [ [ INF for _ in range(cap + 1) ] for _ in range(n) ] for _ in range(n) ]
  heap = []
  # initialize first row distances
  for c in range(n):
    if is_empty_or_other_flatstone(state, 0, c, player):
      dist[0][c][0] = 1
      heapq.heappush(heap, (dist[0][c][0], 0, c, 0))
    elif is_our_flatstone(state, 0, c, player):
      dist[0][c][0] = 0
      heapq.heappush(heap, (dist[0][c][0], 0, c, 0))
    elif is_starndingstone(state, 0, c):
      dist[0][c][1] = 1
      heapq.heappush(heap, (dist[0][c][1], 0, c, 1))
    else:
      assert False
  while len(heap) > 0:
    cur = heapq.heappop(heap)
    r = cur[1]
    c = cur[2]
    cp = cur[3]
    for d in DELTA:
      rr = r + d[0]
      cc = c + d[1]
      if not (0 <= rr and rr < n and 0 <= cc and cc < n): continue
      if is_standingstone(state, rr, cc) and state.capstones[player] - cp > 0 and dist[r][c][cp] + 1 < dist[rr][cc][cp + 1]:
        # position (rr, cc) contains a standing stone and we still have capstones left
        dist[rr][cc][cp + 1] = dist[r][c][cp] + 1
        heapq.heappush(heap, (dist[rr][cc][cp + 1], rr, cc, cp + 1))
      elif is_empty_or_other_flatstone(state, rr, cc, player) and dist[r][c][cp] + 1 < dist[rr][cc][cp]:
        # position (rr, cc) is either empty of contains a non standing piece from the other player
        dist[rr][cc][cp] = dist[r][c][cp] + 1
        heapq.heappush(heap, (dist[rr][cc][cp], rr, cc, cp))
      elif is_our_flatstone(state, r, c, player) and dist[r][c][cp] + 0 < dist[rr][cc][cp]:
        # position (rr, cc) is already ours
        dist[rr][cc][cp] = dist[r][c][cp] + 0
        heapq.heappush(heap, (dist[rr][cc][cp], rr, cc, cp))
  for c in range(n):
    print(dist[n - 1][c][0])
    print(dist[n - 1][c][1])
    print()

"""
Compute the minimum number of pieces needed for the given palyer
to connect the bottom and the top.
"""
def vertical_dist(state, player):
  return dijkstra(state, player, TOP, BOTTOM)

"""
Compute the minimum number of pieces needed for the given palyer
to connect the left and the right.
"""
def horizontal_dist(state, player):
  return dijkstra(state, player, LEFT, RIGHT)


def dijkstra(state, player, orig, dest):
  n = state.get_size()
  dist = [ [ INF for _ in range(n) ] for _ in range(n) ]
  heap = []
  # initialize first row distances
  for r, c in orig:
    if is_empty_or_other_flatstone(state, r, c, player):
      dist[r][c] = 1
      heapq.heappush(heap, (dist[r][c], r, c))
    elif is_our_flatstone(state, r, c, player):
      dist[r][c] = 0
      heapq.heappush(heap, (dist[r][c], r, c))
  # dijkstra loop 
  while len(heap) > 0:
    cur = heapq.heappop(heap)
    r = cur[1]
    c = cur[2]
    for d in DELTA:
      rr = r + d[0]
      cc = c + d[1]
      if not (0 <= rr and rr < n and 0 <= cc and cc < n) or is_standingstone(state, rr, cc): continue
      cost = 0
      if is_empty_or_other_flatstone(state, rr, cc, player):
        cost += 1
      if dist[r][c] + cost < dist[rr][cc]:
        dist[rr][cc] = dist[r][c] + cost
        heapq.heappush(heap, (dist[rr][cc], rr, cc))
  best = INF
  for r, c in dest:
    best = min(best, dist[r][c])
  return best

def is_empty_or_other_flatstone(state, r, c, player):
  if state.is_empty(r, c): return True
  tp, ow = state.get_top_piece(r, c)
  if ow != player and tp != STANDING_STONE: return True
  return False

def is_our_flatstone(state, r, c, player):
  return not state.is_empty(r, c) and state.get_top_piece(r, c)[1] == player

def is_standingstone(state, r, c):
  return not state.is_empty(r, c) and state.get_top_piece(r, c)[0] == STANDING_STONE
    

"""
print(state)
vertical = top_bottom_distance_simple(state, 0)
horizontal = left_right_distance_simple(state, 0)
print(vertical, horizontal)
"""
